package pageObjs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class NewsPagePO {

	
	public static WebElement LatestNews(WebDriver driver) 
	{
		String locator = "//p[contains(text(),'latest news')]";
		WebElement ele1 = driver.findElement(By.xpath(locator));
		return ele1;
	}
}
