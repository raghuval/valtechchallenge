package pageObjs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.*;

public class MenusPO {
	
	static Common com = new Common();
	public static WebElement OtherMenu(WebDriver driver)
	{
		String locator = "//*[@class='header__navigation_trigger-icons']//label[2]/span/div";
		
		
		WebElement ele = driver.findElement(By.xpath(locator));
		
		return ele;
	}
	
	public static WebElement ContactMenu(WebDriver driver)
	{
		String locator = "//*[@class='header__navigation_trigger-icons']//label[1]/div/a";
		WebElement ele = driver.findElement(By.xpath(locator));
		return ele;
	}
	
	public static WebElement CrossMenu(WebDriver driver)
	{
		//String locator = "//i[@data-icon='cross']";
		String locator = "//*[contains(@id,'navigation-toggle-button')]";
		WebElement ele = driver.findElement(By.xpath(locator));
		return ele;
	}
	
	public static WebElement PageElementonMenu(WebDriver driver, String pagename )
	{
		//String locator = "//*[contains(text(),'Who we are')]/..//a[contains(text(),'News')]";
		//String locator = "//a[contains(text(),'"+pagename+"')]";
		String locator = "//a[contains(text(),'"+pagename+"') and @title='"+pagename+"']";
		//WaitforElementToBePresent(locator,20);
		WebElement ele1 = driver.findElement(By.xpath(locator));
		return ele1;
	}
	
	
	
	
	

}
