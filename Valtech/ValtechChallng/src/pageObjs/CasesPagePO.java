package pageObjs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CasesPagePO {
	
	
	
	public static WebElement H1Tag(WebDriver driver)
	{
		//String locator = "//input[@name='username']";
		String locator = "//h1";
		
		//WaitforElementToBePresent(locator,20);
		WebElement ele1 = driver.findElement(By.xpath(locator));
		return ele1;
	}

}
