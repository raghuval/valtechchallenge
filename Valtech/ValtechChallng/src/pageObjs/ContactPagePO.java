package pageObjs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContactPagePO {
	
	public static int NoOfOfficesInEachCountryElement(WebDriver driver)
	{
		
		String locator = "//div[@class='contactcountry']/ul/li";
		
		//WaitforElementToBePresent(locator,20);
		int  noofele1 = driver.findElements(By.xpath(locator)).size();
		return noofele1;
	}

}
