package ActionsOnPOs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.*;
import pageObjs.*;

public class CommonActions {
	
	static MenusPO menuobj = new MenusPO();
	
	public static void clickoncross(WebDriver driver)
	{
		WebElement crossele = menuobj.CrossMenu(driver);
		crossele.click();
		
	}
	
	public static void clickonothermenu(WebDriver driver)
	{
		WebElement othermenuele = menuobj.OtherMenu(driver);
		othermenuele.click();
		
	}
	
	public static void clickonContactmenu(WebDriver driver)
	{
		WebElement contactmenuele = menuobj.ContactMenu(driver);
		contactmenuele.click();		
	}
	
	public static void clickonpageinmenu(WebDriver driver, String nameofpage) throws InterruptedException
	{
		WebElement pageelementonMenu = menuobj.PageElementonMenu(driver, nameofpage);
		Thread.sleep(3000);
		pageelementonMenu.click();
	}
	
	

}
